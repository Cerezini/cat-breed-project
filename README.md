# Cat Breed Project

This application consists of 3 web pages: Home, Search Results and Breed Details.

- Home: displays 5 random cat breeds and there is a search box to find breeds by their name.
- Search Results: Shows all the cat breeds found that contain the name searched.
- Breed Details: Details of a specific cat breed.

## Project

The project was built over [yii 2](https://www.yiiframework.com/) framework and [bootstrap 4](https://getbootstrap.com/).

All cat breed information and pictures were provided by the [TheCatApi](https://thecatapi.com/).

## Views

The 3 pages mentioned are represented in the files:

- Home: `index.php`
- Search Results: `breed-search.php`
- Breed Details: `breed-details.php`

Bootstrap grid classes were used to make the pages responsive.

## Models

- CatBreed: The object that contains all information of a cat breed returned by TheCatApi, plus a field `imageUrl` to store one breed image url.
- CatBreedSearchForm: The object that contains the field `name` used in the search form.

## Controller

All actions are in the `SiteController.php`:

- Index: Responsible for rendering the home page with 5 random cat breeds and a search box.
- BreedSearch: Receives a POST request with a `name` field which it is used to search breeds that contains this name and render the page with the results.
- BreedDetails: It receives a `breedId` parameter from the GET request which is used to get one specific breed and render the page with all the breed details.

## Services

2 services were created: **CatBreedService** and **CatApiService**. CatApiService is responsible for the requests to TheCatApi. CatBreedService uses the results returned by the CatApiService to map them to the CatBreed model and return the expected responses to the controller.

### CatBreedService methods

- `getBreed(string $breedId)`: get a CatBreed by its id.
- `getFiveRandomBreeds()`: get 5 random breeds. As the api doesn`t have an endpoint for that, the solution was to bring all and select randomly. This solution considered the number of items (~67) returned, for a different scenario maybe a different approach would be necessary.
- `searchBreedsByName(string $name)`: get a list of CatBreeds that contains in its name the searched string.

### CatApiService methods

- `getAllBreeds()`: get all breeds.
- `getBreed(string $breedId)`: get a breed by id.
- `getBreedImage(string $breedId)`: get one random image for a cat breed.
- `searchBreedsByName(string $name)`: gets a list of breeds by the searched name.

## Setup

In the root directory of the project

```bash
docker network create cats

docker-compose up -d

docker-compose exec app bash

composer install
```

The app should be available at `http://localhost:8000`