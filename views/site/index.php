<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Cat Breeds';
?>
<div class="site-index">
        
    <h1 class="text-center">Cat Breeds</h1>

    <div class="body-content">

        <div class="row mt-20">

            <div class="col-lg-12">
            
                <?php $form = ActiveForm::begin([
                    'action' => ['site/breed-search']
                ]); ?>

                    <?= $form->field($catBreedSearch, 'name') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            
            </div>

        </div>

        <div class="row mt-20 justify-content-center">
            
            <?php foreach($catBreeds as $catBreed):?>

                <div class="col-lg-4 mt-20">
                    <a href=<?= Html::encode("?r=site/breed-details&breedId=" . $catBreed->id) ?>>
                        <div class="card text-center">

                            <div class="card-header">
                                <?= Html::encode($catBreed->name) ?>
                            </div>

                            <?php if (empty($catBreed->imageUrl)): ?>

                                <?= Html::img('no-image.png', [
                                    'alt' => $catBreed->name,
                                    'class' => 'card-img-bottom img-thumbnail'
                                ]) ?>

                            <?php else: ?>

                                <?= Html::img($catBreed->imageUrl, [
                                    'alt' => $catBreed->name,
                                    'class' => 'card-img-bottom img-thumbnail'
                                ]) ?>

                            <?php endif; ?>
                            
                        </div>
                    </a>
                </div>
                
            <?php endforeach;?>

        </div>

    </div>
</div>
