<?php

use yii\helpers\Html;

$this->title = 'Cat Breeds';
?>
<div class="site-index">

    <?php if (count($catBreeds) > 0):?>
        <h1 class="text-center">Search for '<?= Html::encode($search)?>'</h1>
    <?php else:?>
        <h1 class="text-center">No cat breeds found for '<?= Html::encode($search)?>'</h1>
    <?php endif;?>

    <div class="body-content">

        <div class="row mt-40">
            
            <?php foreach($catBreeds as $catBreed):?>
                
                <div class="col-lg-3 mt-20">
                    <a href=<?= Html::encode("?r=site/breed-details&breedId=" . $catBreed->id) ?>>
                        <div class="card text-center">

                            <div class="card-header">
                                <?= Html::encode($catBreed->name) ?>
                            </div>

                            <?php if (empty($catBreed->imageUrl)): ?>

                                <?= Html::img('no-image.png', [
                                    'alt' => $catBreed->name,
                                    'class' => 'card-img-bottom img-thumbnail'
                                ]) ?>

                            <?php else: ?>

                                <?= Html::img($catBreed->imageUrl, [
                                    'alt' => $catBreed->name,
                                    'class' => 'card-img-bottom img-thumbnail'
                                ]) ?>

                            <?php endif; ?>
                            
                        </div>
                    </a>
                </div>
                
            <?php endforeach;?>

        </div>

    </div>
</div>
