<?php

use yii\helpers\Html;

$this->title = 'Cat Breeds';
?>
<div class="body-content">

    <div class="row">
          
        <div class="col-12 text-center">
            <?= Html::tag('h1', Html::encode($catBreed->name)) ?>
        </div>

        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center mt-40">
            <?php if (empty($catBreed->imageUrl)): ?>

                <?= Html::img('no-image.png', [
                    'alt' => $catBreed->name,
                    'class' => 'img-fluid img-thumbnail'
                ]) ?>

            <?php else: ?>

                <?= Html::img($catBreed->imageUrl, [
                    'alt' => $catBreed->name,
                    'class' => 'img-fluid img-thumbnail'
                ]) ?>

            <?php endif; ?>
            
        </div>

        <div class="col-12 mt-40"></div>

        <?php if (!empty($catBreed->alt_names)): ?>
            <?= Html::tag('dt', 'Alternative Names', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->alt_names), ['class' => 'col-lg-6']) ?>
        <?php endif; ?>        

        <?php if (!empty($catBreed->origin)): ?>
            <?= Html::tag('dt', 'Origin', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->origin), ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->description)): ?>
            <?= Html::tag('dt', 'Description', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->description), ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->temperament)): ?>
            <?= Html::tag('dt', 'Temperament', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->temperament), ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->life_span)): ?>
            <?= Html::tag('dt', 'Life Span', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->life_span) . ' years', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->weight_imperial)): ?>
            <?= Html::tag('dt', 'Weight', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->weight_imperial) . ' pounds', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <div class="col-12 mt-20"></div>

        <?php if ($catBreed->hasCharacteristics()): ?>
            <?= Html::tag('dt', 'Characteristics', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <dd class="col-lg-6">
                
                <?php if (!empty($catBreed->experimental)): ?>
                    <p>Experimental</p>
                <?php endif; ?>

                <?php if (!empty($catBreed->hairless)): ?>
                    <p>Hairless</p>
                <?php endif; ?>

                <?php if (!empty($catBreed->hypoallergenic)): ?>
                    <p>Hypoallergenic</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->indoor)): ?>
                    <p>Indoor</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->lap)): ?>
                    <p>Lap</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->natural)): ?>
                    <p>Natural</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->rare)): ?>
                    <p>Rare</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->rex)): ?>
                    <p>Rex</p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->short_legs)): ?>
                    <p>Short Legs</p>
                <?php endif; ?>

                <?php if (!empty($catBreed->supress_tail)): ?>
                    <p>Supress Tail</p>
                <?php endif; ?>

            </dd>
        <?php endif; ?>
        
        <div class="col-12 mt-20"></div>

        <?php if (!empty($catBreed->adaptability)): ?>
            <?= Html::tag('dt', 'Adaptability', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->adaptability) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->affection_level)): ?>
            <?= Html::tag('dt', 'Affection Level', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->affection_level) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
    
        <?php if (!empty($catBreed->child_friendly)): ?>
            <?= Html::tag('dt', 'Child Friendly', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->child_friendly) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
    
        <?php if (!empty($catBreed->dog_friendly)): ?>
            <?= Html::tag('dt', 'Dog Friendly', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->dog_friendly) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
    
        <?php if (!empty($catBreed->energy_level)): ?>
            <?= Html::tag('dt', 'Energy Level', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->energy_level) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->grooming)): ?>    
            <?= Html::tag('dt', 'Grooming', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->grooming) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <?php if (!empty($catBreed->health_issues)): ?>   
            <?= Html::tag('dt', 'Health Issues', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->health_issues) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
        
        <?php if (!empty($catBreed->intelligence)): ?>
            <?= Html::tag('dt', 'Intelligence', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->intelligence) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
        
        <?php if (!empty($catBreed->shedding_level)): ?>
            <?= Html::tag('dt', 'Shedding Level', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->shedding_level) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
        
        <?php if (!empty($catBreed->social_needs)): ?>
            <?= Html::tag('dt', 'Social Needs', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->social_needs) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
        
        <?php if (!empty($catBreed->stranger_friendly)): ?>
            <?= Html::tag('dt', 'Stranger Friendly', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->stranger_friendly) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>
        
        <?php if (!empty($catBreed->vocalisation)): ?>
            <?= Html::tag('dt', 'Vocalisation', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <?= Html::tag('dd', Html::encode($catBreed->vocalisation) . ' / 5', ['class' => 'col-lg-6']) ?>
        <?php endif; ?>

        <div class="col-12 mt-20"></div>

        <?php if ($catBreed->hasLinks()): ?>
            <?= Html::tag('dt', 'Links', ['class' => 'col-lg-3 offset-lg-3']) ?>
            <dd class="col-lg-6">

                <?php if (!empty($catBreed->cfa_url)): ?>
                    <p><?= Html::a('CFA', Html::encode($catBreed->cfa_url)) ?></p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->vcahospitals_url)): ?>
                    <p><?= Html::a('VCA Hospitals', Html::encode($catBreed->vcahospitals_url)) ?></p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->vetstreet_url)): ?>
                    <p><?= Html::a('VetStreet', Html::encode($catBreed->vetstreet_url)) ?></p>
                <?php endif; ?>
                
                <?php if (!empty($catBreed->wikipedia_url)): ?>
                    <p><?= Html::a('Wikipedia', Html::encode($catBreed->wikipedia_url)) ?></p>
                <?php endif; ?>

            </dd>
        <?php endif; ?>

    </div>

</div>