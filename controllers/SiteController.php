<?php

namespace app\controllers;

use app\models\CatBreedSearchForm;
use app\services\CatBreedService;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(): string
    {
        $catBreedSearch = new CatBreedSearchForm();

        $catBreedService = new CatBreedService();
        $catBreeds = $catBreedService->getFiveRandomBreeds();

        return $this->render('index', [
            'catBreeds' => $catBreeds,
            'catBreedSearch' => $catBreedSearch
        ]);
    }

    public function actionBreedDetails(string $breedId): string
    {
        if (empty($breedId)) {
            throw new BadRequestHttpException('Invalid breedId');
        }

        $catBreedService = new CatBreedService();
        $catBreed = $catBreedService->getBreed($breedId);

        if ($catBreed == null) {
            throw new NotFoundHttpException('No cat breed found');
        }

        return $this->render('breed-details', ['catBreed' => $catBreed]);
    }

    public function actionBreedSearch(): string
    {
        $catBreedSearch = new CatBreedSearchForm();

        $catBreedSearch->load(Yii::$app->request->post());

        if (!$catBreedSearch->validate()) {
            throw new BadRequestHttpException();
        }
        
        $catBreedService = new CatBreedService();
        $catBreeds = $catBreedService->searchBreedsByName($catBreedSearch->name);

        return $this->render('breed-search', [
            'catBreeds' => $catBreeds,
            'search' => $catBreedSearch->name
        ]);
    }
}
