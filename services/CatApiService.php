<?php

namespace app\services;

use Exception;
use GuzzleHttp\Client as GuzzleClient;

class CatApiService
{
    public const url = 'https://api.thecatapi.com/v1';

    public function getAllBreeds(): array
    {
        try {
            $client = new GuzzleClient();
            $res = $client->request('GET', self::url . '/breeds');
            return json_decode($res->getBody()->getContents(), true);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getBreed(string $breedId): array
    {
        try {
            $client = new GuzzleClient();
            $res = $client->request('GET', self::url . '/breeds/' . $breedId);
            return json_decode($res->getBody()->getContents(), true);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getBreedImage(string $breedId): array
    {
        try {
            $client = new GuzzleClient();
            $res = $client->request('GET', self::url . '/images/search?breed_id=' . $breedId);
            $imageResults = json_decode($res->getBody()->getContents(), true);
            return $imageResults[0] ?? $imageResults;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function searchBreedsByName(string $name): array
    {
        try {
            $client = new GuzzleClient();
            $res = $client->request('GET', self::url . '/breeds/search?q=' . $name);
            return json_decode($res->getBody()->getContents(), true);
        } catch (Exception $e) {
            throw $e;
        }
    }
}