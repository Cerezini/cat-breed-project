<?php

namespace app\services;

use app\models\CatBreed;

class CatBreedService
{
    public function getBreed(string $breedId): ?CatBreed
    {
        $catApiService = new CatApiService();
        $breed = $catApiService->getBreed($breedId);

        if (empty($breed)) {
            return null;
        }

        $breedImage = $catApiService->getBreedImage($breedId);

        $catBreed = new CatBreed();
        $catBreed->fillData($breed);
        $catBreed->imageUrl = $breedImage['url'] ?? '';
        $catBreed->weight_imperial = $breed['weight']['imperial'] ?? '';

        return $catBreed;
    }

    public function getFiveRandomBreeds(): array
    {
        $catApiService = new CatApiService();
        $breeds = $catApiService->getAllBreeds();

        $randomIndexes = $this->uniqueRandomNumbersWithinRange(0, count($breeds), 5);
        sort($randomIndexes, SORT_NUMERIC);
        
        $catBreeds = [];

        foreach ($randomIndexes as $index) {
            $breed = $breeds[$index];
            
            $catBreed = new CatBreed();
            $catBreed->id = $breed['id'];
            $catBreed->name = $breed['name'];
            $catBreed->imageUrl = $breed['image']['url'] ?? '';
            
            $catBreeds[] = $catBreed;
        }

        return $catBreeds;
    }

    public function searchBreedsByName(string $name): array
    {
        $catApiService = new CatApiService();
        $breeds = $catApiService->searchBreedsByName($name);

        $catBreeds = [];

        foreach ($breeds as $breed) {
            $breedImage = $catApiService->getBreedImage($breed['id']);

            $catBreed = new CatBreed();
            $catBreed->id = $breed['id'];
            $catBreed->name = $breed['name'];
            $catBreed->imageUrl = $breedImage['url'] ?? '';

            $catBreeds[] = $catBreed;
        }

        return $catBreeds;
    }

    private function uniqueRandomNumbersWithinRange(int $min, int $max, int $quantity): array
    {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }
}