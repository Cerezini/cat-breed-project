<?php

namespace app\models;

use yii\base\Model;

class CatBreed extends Model
{
    public string $id;
    public string $name;
    public string $imageUrl;

    public string $alt_names;
    public string $cfa_url;
    public string $country_code;
    public string $description;
    public string $life_span;
    public string $origin;
    public string $temperament;
    public string $vcahospitals_url;
    public string $vetstreet_url;
    public string $weight_imperial;
    public string $wikipedia_url;
    
    public int $experimental;
    public int $hairless;
    public int $hypoallergenic;
    public int $indoor;
    public int $lap;    
    public int $natural;
    public int $rare;
    public int $rex;
    public int $short_legs;
    public int $supress_tail;
    
    public int $adaptability;
    public int $affection_level;
    public int $child_friendly;
    public int $dog_friendly;
    public int $energy_level;
    public int $grooming;
    public int $health_issues;
    public int $intelligence;
    public int $shedding_level;
    public int $social_needs;
    public int $stranger_friendly;
    public int $vocalisation;

    public function fillData(array $data): void
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function hasCharacteristics(): bool
    {
        return !empty($this->experimental)
            || !empty($this->hairless)
            || !empty($this->hypoallergenic)
            || !empty($this->indoor)
            || !empty($this->lap)
            || !empty($this->natural)
            || !empty($this->rare)
            || !empty($this->rex)
            || !empty($this->short_legs)
            || !empty($this->supress_tail);
    }

    public function hasLinks(): bool
    {
        return !empty($this->cfa_url)
            || !empty($this->vcahospitals_url)
            || !empty($this->vetstreet_url)
            || !empty($this->wikipedia_url);
    }
}